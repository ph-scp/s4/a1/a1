<?php


//Section Objects from Classes


// The keyword "public" below is an example of an access modifier
// properties and methods that are public can be directly accessed
// Keep in mind, when a property or method is accessed, not only can it be
//   displayed, it can alse be reaassigned

class Building
{
  protected $name;
  protected $floors;
  protected $address;

  public function __construct($name, $floors, $address)
  {
    $this->name = $name;
    $this->floors = $floors;
    $this->address = $address;
  }

  //Encapsulation
  /* Encapsulation allows private or protected properties/methods to be Indirectly 
  printed or changed with the use 
  */
  //getter allow indirect access to print out values
  //getter names are user-defined (can be anything)

  public function getName()
  {
    return  $this->name;
  }

  //setter allow indirect access to reassign values
  public function setName($name)
  {
    $this->name = $name;
  }


  public function getFloors()
  {
    return  $this->floors;
  }

  //setter allow indirect access to reassign values
  public function setFloors($floors)
  {
    $this->floors = $floors;
  }

  public function getAddress()
  {
    return  $this->address;
  }

  //setter allow indirect access to reassign values
  public function setAddress($address)
  {
    $this->address = $address;
  }

  public function printName()
  {
    return "The name of the building is $this->name";
  }

  public function printFloors()
  {
    return "The $this->name has $this->floors.";
  }

  public function printAddress()
  {
    return "The $this->name is located at $this->address";
  }
}

class Condominium extends Building
{
  public function printName()
  {
    return "The name of the codiminiun is $this->name";
  }

  public function printFloors()
  {
    return "The $this->name has $this->floors.";
  }

  public function printAddress()
  {
    return "The $this->name is located at $this->address";
  }
}

//Object creation from a class
$building = new Building('Caswynn Building', 8, 'Quezon City, Philippines');
$condominium = new Condominium('Enzo Condo', 5, "Makati City, Philippines");
