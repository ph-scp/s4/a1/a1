<?php require_once "./code.php";


?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>S4: Access Modifiers and Encapsulation</title>
</head>

<body>

  <h1>Building</h1>
  <p> The name of Building is <?= $building->getName(); ?></p>

  <p>The <?= $building->getName(); ?> has<?= $building->getFloors(); ?></p>
  <p>The <?= $building->getName(); ?> is located at <?= $building->getAddress(); ?></p>
  <p><?= $building->setName("Caswynn Complex"); ?></p>
  <p>The of building has change to <?= $building->getName(); ?></p>

  <h1>Condominium</h1>
  <p> The name of condominium is <?= $condominium->getName(); ?></p>

  <p>The <?= $condominium->getName(); ?> has<?= $condominium->getFloors(); ?></p>
  <p>The <?= $condominium->getName(); ?> is located at <?= $condominium->getAddress(); ?></p>
  <p><?= $condominium->setName("Enzo Tower"); ?></p>
  <p>The of condominium has change to <?= $condominium->getName(); ?></p>

</body>

</html>